<!--https://www.youtube.com/watch?v=gXkqy0b4M5g
-->

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" contect="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=ledge">
	<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/mpstyle.css">
	<title>Main Page</title>
</head>
<body>
	<nav>
		<div class="logo"> </div>
			<h4>Daily Jobs</h4>
		</div>
		<ul class="nav-links">
			<li style="display" id="mainPage"><a href="mainpage.php">Home</a></li>
			<li style="display" id="loginPage"><a href="index.php">Login</a></li>
			<li style="display" id="registerPage"><a href="register.php">Register</a></li>
			<li><a href="#">Opening</a></li>
			<li><a href="#">Status</a></li>
			<li><a href="#">Post Jobs</a></li>
		</ul>
		<div class="burger">
			<div class="line1"> </div>
			<div class="line2"> </div>
			<div class="line3"> </div>
			
		</div>
	</nav>	
	<script src="js\app.js"> </script>
	<script>
		var url=window.location.pathname;
		var trm = url.substring(url.lastIndexOf('/')+1);
		//document.write(trm);
		if(trm=="mainpage.php"){
			document.getElementById("mainPage").style.display = "none";
		}else if(trm=="index.php"){
			document.getElementById("loginPage").style.display = "none";
		}else if(trm=="register.php"){
			document.getElementById("registerPage").style.display = "none";
		}
	</script>
</body>
</html>