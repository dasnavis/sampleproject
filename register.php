<?php include "mainpage.php" ?>
<?php
	require 'dbconfig/config.php';
?>
<!DOCTYPE html>
<html>
<head>
 <title>Register Page</title>
 <link rel="stylesheet" href="css/style.css">
</head>
<body style="background-color:#74b9ff">
<div id="main-wrapper">
	<center>
		<h2> Register </h2>
		<img src="imgs/loginImgs.png" class="avatar">
	</center>
<form class="myform" action="register.php" method="post">
<table>
	<tr>
		<td> <label class="labelvalues"><b>First Name</b></label> </td>
		<td> <input name="firstname" type="text" class="inputvalues" placeholder="Your full name" required /></td>
		<td> <label class="labelvalues"><b>Gender</b></label> </td>
		<td> <select id="gender" name="gender" class="gender">
			 <option value="blank" placeholder="Select"></option>
			 <option value="male">Male</option>
			 <option value="female">Female</option>
		</td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Last Name</b></label></td>
		<td> <input name="lastname" type="text" class="inputvalues" placeholder="Your father's name" required /></td>
		<td> <label class="labelvalues"><b>DOB</b></label></td>
		<td> <input name="dob" type="date" class="dob" placeholder="YYYY-MM-DD" required /></td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Email Id</b></label> </td>
		<td> <input name="emailid" type="text" class="inputvalues" placeholder="Your email address" required /> </td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Mobile No</b></label> </td>
		<td> <input name="mobile" type="text" class="inputvalues" placeholder="Your mobile number" required /> </td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Location</b></label> </td>
		<td> <input name="location" type="text" class="inputvalues" placeholder="Your work location" required /> </td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Company</b></label> </td>
		<td> <input name="company" type="text" class="inputvalues" placeholder="Your current company" required /> </td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Password</b></label> </td>
		<td> <input name="password" type="password" class="inputvalues" placeholder="New password" required /> </td>
	</tr>
	<tr>
		<td> <label class="labelvalues"><b>Confirm Password</b></label> </td>
		<td> <input name="cpassword" type="password" class="inputvalues" placeholder="Confirm password" required /> </td>
	</tr>

	<tr> 
		<td> <input name="submit_btn" type="submit" id="signup_btn" value="Sign Up" /> </td>
		<td> <input name="back" type="submit" id="back_btn" value="Back" onClick="document.location.href='index.php';" /> </td>
	</tr>
</table>
</form>
</div>
<?php
if(isset($_POST['submit_btn'])){
	//echo '<script type="text/javascript"> alert("Sign Up button clicked") </script>';
	$firstname=$_POST["firstname"];
	$lastname=$_POST["lastname"];
	$emailid=$_POST["emailid"];
	$mobile=$_POST["mobile"];
	$location=$_POST["location"];
	$company=$_POST["company"];
	$gender=$_POST["gender"];
	$dob=$_POST["dob"];
	$password=$_POST["password"];
	$cpassword=$_POST["cpassword"];
	
	if(filter_var($emailid, FILTER_VALIDATE_EMAIL)){
		if($password==$cpassword){
			$query="select * from user where firstname='$firstname' and emailid='$emailid'";
			$query_run=mysqli_query($con,$query);
				if(mysqli_num_rows($query_run)>0){
					//there is already a user with  the same firstname
					echo '<script type="text/javascript"> alert("firstname already exist") </script>';
					}else{
						$query="insert into user values('$firstname','$lastname','$emailid','$mobile','$location','$company','$gender','$dob','$password')";
						$query_run=mysqli_query($con,$query);
							if($query_run){
								echo '<script type="text/javascript"> alert("User Registered.. Go to login page") </script>';
							}else{
								echo '<script type="text/javascript"> alert("Error") </script>';
							}
						}
			}else{
				echo '<script type="text/javascript"> alert("Password and confirmation password does not matches OR Invalid emailId") </script>';
			}
}else{// main isset "IF" condition
	echo '<script type="text/javascript"> alert("Invalid Email Address") </script>';
	}
}
?>
</body>
</html>